package es.batoi.caso04_minieditor;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * FXML Controller class
 *
 * @author sergio
 */
public class AppController implements Initializable {

    @FXML
    private TextArea taEditor;
    @FXML
    private Label lblInfo;
    @FXML
    private Button btnAbrir;
    @FXML
    private Button btnCerrar;
    @FXML
    private Button btnGuardar;
    @FXML
    private Button btnNuevo;

    private Stage escenario;
    private File f;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        taEditor.setDisable(false);
        taEditor.textProperty().addListener((observableValue, s, t1) -> estado());
    }

    @FXML
    private void handleNuevo() {
        taEditor.clear();
    }

    @FXML
    private void handleAbrir() throws IOException {
        FileChooser fc = new FileChooser();
        fc.setTitle("ABRIR");
        f = fc.showOpenDialog(escenario);
        File file = new File(f.getName());
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        StringBuilder text = new StringBuilder();

        int contador;

        while ((contador = br.read()) != -1) {
            text.append((char) contador);
        }
        br.close();
        taEditor.setText(text.toString());
    }

    @FXML
    private void handleGuardar() throws IOException {
        FileChooser fc = new FileChooser();
        fc.setTitle("GUARDAR");
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("text file", "*.txt"));
        File file = new File(f.getName());
        FileWriter fw = new FileWriter(file);
        BufferedWriter bw = new BufferedWriter(fw);
    }

    @FXML
    private void handleCerrar() {
        Platform.exit();
    }


    void setEscenario(Stage escenario) {
        this.escenario = escenario;
    }

    private void estado() {
        int caracteres = taEditor.getLength();
        int lineas = taEditor.getParagraphs().size();
        lblInfo.setText(caracteres + " caracteres  /" + lineas + " lineas");
    }


}
